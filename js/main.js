(function ($) {
    "use strict";

    jQuery(document).ready(function ($) {


        $(".embed-responsive iframe").addClass("embed-responsive-item");
        $(".carousel-inner .item:first-child").addClass("active");

        $('[data-toggle="tooltip"]').tooltip();



        $(".slider").owlCarousel({
            items: 1,
            autoplay: true,
            loop: true,
            smartSpeed: 1000,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
        });

        $(".slider").on('translate.owl.carousel', function () {
            $('.sl-cntn h3').removeClass('fadeInRight animated').hide();
            $('.sl-cntn h1').removeClass('fadeInDown animated').hide();
            $('.sl-cntn h2').removeClass('fadeInLeftBig animated').hide();
        });
        $(".slider").on('translated.owl.carousel', function () {
            $('.owl-item.active .sl-cntn h3').addClass('fadeInRight animated').show();
            $('.owl-item.active .sl-cntn h1').addClass('fadeInDown animated').show();
            $('.owl-item.active .sl-cntnn h2').addClass('fadeInRightBig animated').show();
        });


        /* About area Start */

        $('.about-menu ul li').click(function () {
            $('.about-menu ul li').removeClass('active');
            $(this).addClass('active');

            var selector = jQuery(this).attr('data-filter');
            $('.col-md-12').isotope({
                filter: selector,
                itemSelector: '.catagory',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false,
                }
            });
            return false;

        });



        $('.col-md-12').isotope({
            itemSelector: '.catagory'
        });


        /* About area End */


        $(function () {
            $(".meter span").each(function () {
                $(this)
                    .data("origWidth", $(this).width())
                    .width(0)
                    .animate({
                        width: $(this).data("origWidth")
                    }, 1200);
            });
        });


        /* Protfolio area Start */


        $('.protfolio-iso li').click(function () {
            $('.protfolio-iso li').removeClass('active');
            $(this).addClass('active');

            var selector = jQuery(this).attr('data-filter');
            $('.protfolio').isotope({
                filter: selector,
                itemSelector: '.col-md-3',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false,
                }
            });
            return false;

        });



        $('.protfolio').isotope({
            itemSelector: '.col-md-3'
        });


        /* Protfolio area End */


        $(window).load(function () {
            $(".header-top").sticky({
                
            });
        });
        
        

    });


    jQuery(window).load(function () {


    });


}(jQuery));